<?php

namespace App\Helpers;

use App\Models\AirdropModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class AppHelper
{
    public static function AirdropValidate()
    {
        $arr = [
            'name' => ['required', 'max:200', 'regex:/^[a-zA-Z0-9\ \#\@\*\_\-]+$/u'],
            'description' => 'nullable|regex:/^[^\"' . "\'" . ']+$/u',
            'distribution_date' => 'required|date_format:d-m-Y|after:today',
            'rate' => 'required|numeric|max:100',
        ];
        return $arr;
    }
    public static function CarbonDiff($date)
    {
        $date = Carbon::parse($date);
        $now = Carbon::now();
        $week = $date->diffInWeeks($now);
        $day = $date->diffInDays($now);
        $hour = $date->diffInHours($now);

        if ($week != 0) {
            return $week . ' week left.';
        }
        if ($day != 0) {
            return $day . ' day left.';
        }
        if ($day == 0 && $week == 0) {
            return $hour . ' hour left.';
        }
    }
    public static function CheckNotif()
    {
        $getcook = Cookie::get('checkNotif');
        if (!$getcook) {
            $get = AirdropModel::where('distribution', '<=', Carbon::now())->get()->where('active', 1);
            $count = $get->count();
            if ($count != 0) {
                foreach ($get as $data) {
                    $id = $data->id;
                    $idarr[] = $id;
                    $ins = [
                        'message' => $data->name . ' distribution started!',
                        'data' => \json_encode(['id' => $id]),
                    ];
                    DB::table('notification')->insert($ins);
                }
                AirdropModel::whereIn('id', $idarr)->update(['active' => 0]);
            }
            Cookie::queue('checkNotif', 1, 60);
        }
    }
}
