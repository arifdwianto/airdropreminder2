<?php

namespace App\Http\Controllers;

use App\Helpers\AppHelper;
use App\Models\Airdrop;
use App\Models\AirdropModel;
use App\Models\AuthModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AirdropController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AuthModel $a)
    {
        AppHelper::CheckNotif();
        // return view("dashboard");
        return \view('page-wire', [
            'pageWire' => 'page-dashboard',
            'name' => 'Dashboard',
            // 'pathName' => 'Page Settings',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data_list()
    {
        return \view('page-wire', [
            'pageWire' => 'page-list',
            'name' => 'Projects',
            'title' => 'PROJECT LIST',
            'pathName' => 'Project Lists',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function data_create(Request $request)
    {
        return \view('page-wire', [
            'pageWire' => 'page-create',
            'name' => 'Projects',
            'title' => 'NEW PROJECT',
            'pathName' => 'Add New Project',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Airdrop  $airdrop
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request, AirdropModel $a)
    {
        $name = AirdropModel::where('id', $id)->first();

        if ($name) {
            return \view('page-wire', [
                'pageWire' => 'page-show',
                'name' => 'Project Details',
                'pathName' => 'ID: ' . $name->name,
                // 'ids' => $id,
            ]);} else {
            return \abort(404);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Airdrop  $airdrop
     * @return \Illuminate\Http\Response
     */
    public function setting()
    {
        return \view('page-wire', [
            'pageWire' => 'page-settings',
            'name' => 'Settings',
            'title' => 'SETTINGS',
            'pathName' => 'Page Settings',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Airdrop  $airdrop
     * @return \Illuminate\Http\Response
     */
    public function notif(Request $request)
    {
        return \view('page-wire', [
            'pageWire' => 'page-notif',
            'name' => 'Notification',
            'title' => 'NOTIFICATIONS',
            'pathName' => 'Notification Lists',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Airdrop  $airdrop
     * @return \Illuminate\Http\Response
     */
    public function update(AirdropModel $a, $id)
    {
        $name = AirdropModel::where('id', $id)->first();

        if ($name) {
            return \view('page-wire', [
                'pageWire' => 'page-edit',
                'name' => 'Edit Project',
                'title' => \strtoupper($name->name),
                'pathName' => 'ID: ' . $name->name,
                // 'ids' => $id,
            ]);} else {
            return \abort(404);
        }
    }

    /**
     * LOGIN
     *
     */
    public function login(DB $db, Auth $auth)
    {
        echo \view('airdrop.login');
    }
}
