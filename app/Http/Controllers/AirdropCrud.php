<?php

namespace App\Http\Controllers;

use App\Helpers\AppHelper;
use App\Http\Middleware\VerifyCsrfToken;
use App\Models\AirdropModel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Carbon;
use Illuminate\Validation\ValidationException;

class AirdropCrud extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->input('q', '');
        $page = ($request->input('page', '1') - 1);
        $order_by = $request->input('order_by', 'id-DESC');
        $order_by = \explode('-', $order_by);
        $isiHalaman = 15;
        $skip = $isiHalaman * $page;
        $query = AirdropModel::where('name', 'LIKE', '%' . $search . '%')->orderBy($order_by[0], $order_by[1]);

        //add query

        $count = $query->get()->count();
        $maxPage = ceil($count / $isiHalaman);
        if ($maxPage == 0) {
            $maxPage = 1;
        }

        try {
            $data = $query
                ->skip($skip)
                ->take($isiHalaman)
                ->get();
            $dataX = \json_decode($data, true);
            $json = [
                'current_page' => $page + 1,
                'max_page' => $maxPage,
                'count' => $count,
                'data' => $data,
            ];
            return \response()->json($json, 200);
        } catch (Exception $e) {
            $data = [
                'status' => 'error',
                'message' => $e->getMessage(),
            ];
            return \response()->json($data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, AppHelper::AirdropValidate());

        $date = $request->input('distribution_date');

        try {
            $db = new AirdropModel();
            $db->name = $request->input('name');
            $db->description = $request->input('description');
            $db->rate = $request->input('rate');
            $db->distribution = Carbon::parse($date)->format('Y-m-d H:i:s');
            $db->links = base64_encode($request->input('link'));
            $db->save();
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                throw ValidationException::withMessages([
                    'name' => 'Airdrop name already in use.',
                ]);
            }
        }

        return $request->input();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, AirdropModel $db)
    {
        $this->validate($request, AppHelper::AirdropValidate());
        $date = $request->input('distribution_date');
        $arr = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'rate' => $request->input('rate'),
            'distribution' => Carbon::parse($date)->format('Y-m-d H:i:s'),
            'links' => base64_encode($request->input('link')),
        ];
        try {
            $s = $db->where('id', $id);
            $count = $s->get()->count();
            if ($count != 0) {
                $s->update($arr);
            }
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                throw ValidationException::withMessages([
                    'name' => 'Airdrop name already in use.',
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = AirdropModel::where('id', $id)->
            withCasts([
            'created_at' => 'date:d/m/Y',
            'distribution' => 'date:d/m/Y',
        ])->first();

        $activeText = [
            0 => 'Inactive',
            1 => 'Active',
        ];

        function textRate($val)
        {
            $col = 'warning';
            ($val <= 40) ? $col = 'danger' : '';
            ($val >= 70) ? $col = 'success' : '';
            return $col;
        }

        $post->activeText = $activeText[$post->active];
        $post->name = \ucwords($post->name);
        $post->rateStatus = textRate($post->rate);

        if ($post) {
            $parr = \json_decode($post, true);
            $post = \array_merge($parr, ['links' => \base64_decode($post->links)]);
        }

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data' => $post,
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, AirdropModel $db, Request $request)
    {
        if ($request->session()->token() === $request->input('_token')) {
            try {
                $get = $db->where('id', $id)->firstOrFail();
                ($get == true) ? $db->destroy($id) : "";

                return \response()->json([], 200);
            } catch (QueryException $e) {
                return $e;
            }
        } else {
            return response()->json([
                'messsage' => 'Invalid Token!',
            ], 401);
        }

    }

    public function cree(Request $req)
    {
        return \json_encode($req);
    }
}
