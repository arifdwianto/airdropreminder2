<?php

namespace App\Http\Controllers;

use App\Models\AuthModel;
use App\Models\DBAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AuthModel $a, Request $request)
    {
        $token = $request->input('token');
        if ($token) {
            // TOKEN CHECK
            $cektoken = $a->check($token);
            if ($cektoken == true) {
                $expired = $a->explain($token);
                $cookietime = $expired - time();
                Cookie::queue('session', $token, $cookietime);
                return redirect('/');
            } else {
                return \redirect('auth/error');
            }

        } else {
            return \view('airdrop.login');
        }
        // if (!$a->check() == false) {
        //     return AuthModel::toHome();
        // } else {
        //     return \view('airdrop.login');
        // }
    }

    /**
     * Login validation
     *
     * @return \Illuminate\Http\Response
     */
    public function check(Request $r, Response $re)
    {
        $pass = DBAdmin::first()->password;
        $input = md5($r->input("password"));

        if ($input == $pass) {
            $response = [
                "status" => true,
                "data" => [
                    "token" => AuthModel::generate(),
                ],
            ];
        } else {
            $response = [
                "status" => false,
                "errormsg" => "Invalid Password",
            ];
        }

        return \response()->json($response);

    }

    /**
     * destroy session
     *
     */
    public function logout(Request $request, AuthModel $a)
    {
        Cookie::queue(
            Cookie::forget('session')
        );
        return \redirect('/');
    }

    /**
     * Return error json
     *
     *
     */
    public function error_json()
    {
        $error = [
            'status' => 'error',
            'message' => 'Invalid token!',
        ];
        return \response()->json($error);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
