<?php

namespace App\Http\Livewire;

use App\Models\TableNotification;
use Exception;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Notification extends Component
{
    public $count;
    public $aria = false;

    public function render()
    {
        $unreadNotif = TableNotification::where('unread', 1)->get()->count();
        $dataNotif = TableNotification::orderByDesc('id')->limit(10)->get();

        // $this->aria = 0;
        // $this->count = $unreadNotif;

        return view('livewire.notification', [
            'dataNotif' => $dataNotif,
            'getUnread' => $unreadNotif,
        ]);
    }
    public function mount()
    {
        $this->count = TableNotification::where('unread', 1)->get()->count();

    }
    public function readNewNotif()
    {
        $this->aria = 'true';
        $this->count = 0;
        $this->emit('asyncDone');
    }
    public function unreadData($id)
    {
        $db = TableNotification::where('id', $id)->first();
        $db->update(['unread' => 0]);
    }
    public function markAll()
    {
        DB::table('notification')->update(['unread' => 0]);
    }
}
