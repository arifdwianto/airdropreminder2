<?php

namespace App\Http\Livewire;

use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class PageCreate extends Component
{
    public $name, $description, $distribution_date, $rate = 100;
    public $links;
    public $onelinks;

    public function render()
    {
        return view('livewire.page-create');
    }
    public function mount()
    {
        $this->links = \collect([
            [
                'name' => 'telegram',
                'text' => 'Telegram',
                'placeholder' => 'Enter Telegram Username',
            ],
            [
                'name' => 'twitter',
                'text' => 'Twitter',
                'placeholder' => 'Enter Twitter Username',
            ],
            [
                'name' => 'medium',
                'text' => 'Medium',
                'placeholder' => 'Enter Medium Username',
            ],
            [
                'name' => 'url',
                'text' => 'URL',
                'placeholder' => 'https://www.example.com',
            ],
        ]);

        $this->onelinks = \collect([
            [
                'social' => 'telegram',
                'value' => '',
            ],
        ]);
    }
    public function pushURL($get)
    {
        $pu = $this->links;
        $finds = \array_search($get, \array_column($pu->toArray(), 'name'));
        return $pu[$finds]['placeholder'];
    }
    public function expandURL()
    {
        $one = $this->onelinks;
        $one = $one->toArray();
        \array_push($one, [
            'social' => 'telegram',
            'value' => '',
        ]);
        $this->onelinks = \collect($one);
    }
    public function hitEnter()
    {
        $this->validate(AppHelper::AirdropValidate());
        try {
            $getId = DB::table('airdrop')->insertGetId([
                'name' => $this->name,
                'description' => $this->description,
                'distribution' => Carbon::parse($this->distribution_date)->format('Y-m-d H:i:s'),
                'rate' => $this->rate,
                'links' => base64_encode($this->onelinks),
            ]);
            $this->emit('success', [
                'message' => 'New project has been added to database.',
                'redirect' => "/show/$getId",
            ]);
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                throw ValidationException::withMessages([
                    'name' => 'Airdrop name already in use.',
                ]);
            }
        }
    }
}
