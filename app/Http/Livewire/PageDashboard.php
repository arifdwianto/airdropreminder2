<?php

namespace App\Http\Livewire;

use App\Models\AirdropModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class PageDashboard extends Component
{
    public function render()
    {
        function getCount($val)
        {
            if ($val == 1) {
                $mark = AirdropModel::where('active', $val)->where('distribution', '>=', Carbon::now())->count();
            } else {
                $mark = AirdropModel::where('active', $val)->orWhere('distribution', '<=', Carbon::now())->count();
            }
            return $mark;
        }

        $chart = DB::table('airdrop')
            ->where('created_at', '<=', Carbon::now())
            ->groupBy('date')
            ->orderBy('date', 'DESC')
            ->limit(10)
            ->get([DB::raw('Date(created_at) as date'), DB::raw('COUNT(*) as value')]);

        $distriList = AirdropModel::where('distribution', '>=', Carbon::now())
            ->orderBy('distribution', 'asc')
            ->limit(8)
            ->get();

        $distriList->diff = '200';

        return view('livewire.page-dashboard', [
            'post' => 'abcd',
            'countActive' => getCount(1),
            'countInactive' => getCount(0),
            'chart' => $chart,
            'distributionList' => $distriList,
        ]);
    }
}
