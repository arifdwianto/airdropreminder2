<?php

namespace App\Http\Livewire;

use App\Helpers\AppHelper;
use App\Models\ShowModel;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;
use Livewire\Component;

class PageEdit extends Component
{
    public $name, $description, $distribution_date, $rate;
    public $onelinks;
    public $links;
    public $data = '[]';

    public function render()
    {
        return view('livewire.page-edit');
    }
    public function mount()
    {
        $this->data = ShowModel::find($this->getID());
        $link = $this->data->links;
        $this->onelinks = collect(json_decode(base64_decode($this->data->links)));
        $this->name = $this->data->name;
        $this->rate = $this->data->rate;
        $this->description = $this->data->description;
        $this->links = \collect([
            [
                'name' => 'telegram',
                'text' => 'Telegram',
                'placeholder' => 'Enter Telegram Username',
            ],
            [
                'name' => 'twitter',
                'text' => 'Twitter',
                'placeholder' => 'Enter Twitter Username',
            ],
            [
                'name' => 'medium',
                'text' => 'Medium',
                'placeholder' => 'Enter Medium Username',
            ],
            [
                'name' => 'url',
                'text' => 'URL',
                'placeholder' => 'https://www.example.com',
            ],
        ]);
        $this->distribution_date = Carbon::parse($this->data->distribution)->format('d-m-Y');
    }
    public function getID()
    {
        return Route::getFacadeRoot()->current()->id;
    }
    public function hitEnter()
    {
        $this->validate(AppHelper::AirdropValidate());
        try {
            DB::table('airdrop')->where('id', $this->data->id)->update([
                'name' => $this->name,
                'description' => $this->description,
                'distribution' => Carbon::parse($this->distribution_date)->format('Y-m-d H:i:s'),
                'rate' => $this->rate,
                'links' => base64_encode($this->onelinks),
            ]);
            $this->emit('success', [
                'message' => 'New changes has been saved.',
                'redirect' => "/show/" . $this->data->id,
            ]);
        } catch (QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                throw ValidationException::withMessages([
                    'name' => 'Airdrop name already in use.',
                ]);
            }
        }
    }
}
