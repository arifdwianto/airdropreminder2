<?php

namespace App\Http\Livewire;

use App\Models\AirdropModel;
use App\Models\ShowModel;
use Livewire\Component;
use Livewire\WithPagination;

class PageList extends Component
{
    use WithPagination;
    public $orderBy = 'desc';
    public $orderVal = 'id';
    public $search = '';

    public function render()
    {
        return view('livewire.page-list', [
            'data' => ShowModel::where('name', 'LIKE', '%' . $this->search . '%')->orderBy($this->orderVal, $this->orderBy)->paginate(10),
        ]);
    }
    public function mount()
    {
    }
    public function paginationView()
    {
        return 'livewire.components.paginate';
    }
}
