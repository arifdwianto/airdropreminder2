<?php

namespace App\Http\Livewire;

use App\Models\TableNotification;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class PageNotif extends Component
{
    use WithPagination;
    public $count = 0;
    public $json_data;

    // protected $paginationTheme = 'bootstrap';

    public function render()
    {
        return view('livewire.page-notif', [
            'data' => TableNotification::orderByDesc('id')->paginate(8),
        ]);
    }
    public function mount()
    {
        $db = TableNotification::orderByDesc('id');
        $this->json_data = $db->get();
        $this->count = $db->get()->count();
    }
    public function paginationView()
    {
        return 'livewire.components.paginate';
    }
    public function clearAll()
    {
        DB::table('notification')->delete();
        $this->reset();
    }
}
