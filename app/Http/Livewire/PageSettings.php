<?php

namespace App\Http\Livewire;

use App\Models\AirdropModel;
use App\Models\DBAdmin;
use Livewire\Component;

class PageSettings extends Component
{
    public $currentPassword, $newPassword, $confirmNewPassword, $confirmResetData;
    public $passResetData;

    public function render()
    {
        return view('livewire.page-settings');
    }
    public function changePassword()
    {
        $this->validate([
            'currentPassword' => 'required',
            'newPassword' => 'required|min:8|max:20',
            'confirmNewPassword' => 'required',
        ]);
        if (md5($this->currentPassword) != DBAdmin::first()->password) {
            $this->addError('currentPassword', 'Invalid paassword.');
        }
        if ($this->currentPassword == $this->newPassword) {
            $this->addError('newPassword', 'New password must be different.');
        }
        if ($this->newPassword != $this->confirmNewPassword) {
            $this->addError('confirmNewPassword', 'Enter password confirmation correctly.');
        }
        if ($this->getErrorBag()->isEmpty()) {
            $db = DBAdmin::where('id', 1);
            $db->update(['password' => md5($this->newPassword)]);
            //SAVE DATA
            $this->emit('success', [
                'message' => 'New Password has been saved successfully!',
            ]);
        }
    }
    public function checkResetData()
    {
        $this->validate([
            'passResetData' => 'required',
        ], $messages = [
            'passResetData.required' => 'This field is required.',
        ]);
        if (md5($this->passResetData) != DBAdmin::first()->password) {
            $this->addError('passResetData', 'Invalid paassword.');
        }
        if ($this->getErrorBag()->isEmpty()) {
            $this->emit('confirm-reset');
        }
    }
    public function doResetData()
    {
        $count = AirdropModel::get()->count();
        if ($count != (1 && 0)) {
            AirdropModel::orderBy('id')->limit(($count - 1))->delete();
        }

        $this->emit('success', [
            'message' => 'Data has been reset successfullly',
        ]);
        $this->reset();
    }
}
