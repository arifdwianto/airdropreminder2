<?php

namespace App\Http\Livewire;

use App\Helpers\AppHelper;
use App\Models\AirdropModel;
use App\Models\ShowModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Livewire\Component;

class PageShow extends Component
{
    public $data = '[]';
    public $onelinks;
    public $links;
    // public $id;

    public function render()
    {
        $this->onelinks = collect(json_decode(base64_decode($this->data->links)));
        return view('livewire.page-show');
    }
    public function mount()
    {
        $this->data = ShowModel::find($this->getID());
        $link = $this->data->links;
    }
    public function getID()
    {
        return Route::getFacadeRoot()->current()->id;
    }
    public function showLinks($socialName, $socialValue = 0)
    {
        $iClass = [
            'telegram' => [
                'class' => 'fa-telegram-plane',
                'links' => 'https://t.me/' . $socialValue,
            ],
            'twitter' => [
                'class' => 'fa-twitter',
                'links' => 'https://twitter.com/' . $socialValue,
            ],
            'medium' => [
                'class' => 'fa-medium',
                'links' => "https://$socialValue.medium.com",
            ],
            'url' => [
                'class' => 'fas fa-globe',
                'links' => $socialValue,
            ],
        ];
        return $iClass[$socialName];
    }
    public function Deletes($id, $name)
    {
        DB::table('airdrop')->delete($id);
        $this->emit('success', [
            'message' => base64_decode($name) . " has been removed successfully!",
            'redirect' => '/data',
        ]);

    }
}
