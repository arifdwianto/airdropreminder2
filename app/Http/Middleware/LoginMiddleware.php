<?php

namespace App\Http\Middleware;

use App\Models\AuthModel;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class LoginMiddleware
{
    /**
     * FOR LOGGED CONTROLLER
     */
    public function handle(Request $request, Closure $next)
    {

        return (AuthModel::check(Cookie::get('session'))) ? $next($request) : \redirect('/auth');
    }
}
