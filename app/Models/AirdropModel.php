<?php

namespace App\Models;

use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AirdropModel extends Model
{
    use HasFactory;

    protected $table = 'airdrop';
    protected $appends = [
        'mod_date', 'div_rate', 'active_attr', 'update', 'distance',
    ];
    public function getDistanceAttribute()
    {
        return AppHelper::CarbonDiff($this->distribution);
    }
    public function getModDateAttribute()
    {
        $fmt = 'd F Y';
        $arr = [
            'created_at' => Carbon::parse($this->created_at)->format($fmt),
            'updated_at' => Carbon::parse($this->updated_at)->format($fmt),
            'distribution' => Carbon::parse($this->distribution)->format($fmt),
        ];
        return $arr;
    }
    public function getDivRateAttribute()
    {
        $rate = $this->rate;
        $class = 'warning';
        ($rate >= 70) ? $class = 'success' : '';
        ($rate <= 40) ? $class = 'danger' : '';
        return $class;
    }
    public function getActiveAttrAttribute()
    {
        $a = $this->active;
        $arr = [
            '0' => 'Inactive',
            '1' => 'Active',
        ];
        $col = [
            0 => 'danger',
            1 => 'success',
        ];
        $atr = [
            'class' => $col[$a],
            'text' => $arr[$a],
        ];
        return $atr;
    }
    public function getUpdateAttribute()
    {
        $newdat = Carbon::parse($this->distribution)->format('d-m-Y');
        $arr = [
            'distribution' => $newdat,
        ];
        return $arr;
    }
}
