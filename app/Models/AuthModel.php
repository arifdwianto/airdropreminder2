<?php

namespace App\Models;

use App\Models\Base58;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;

class AuthModel extends Model
{
    use HasFactory;

    protected static $expiry;

    public function __construct()
    {
        static::$expiry = time() + (60 * 60 * 5);
    }

    /*
     * VALIDATE REQUEST
     *
     */
    public static function check($cook = false)
    {
        $session = Cookie::get('session');
        $cook = ($cook == false && $session) ? $cook = $session : $cook;
        $cek1 = ($cook == true) ? true : false; // cek cookie

        return ($cek1 && time() <= Base58::decode($cook)) ? true : false;
    }

    /*
     * BUILD KEY
     *
     */
    public static function explain($token = null)
    {
        return Base58::decode($token);

    }

    /*
     * BUILD KEY
     *
     */
    public static function generate($expiry = 60 * 60 * 5/* 5 hourEXPIRY TIME */)
    {
        ($expiry == null) ? $expiry = self::$expiry : $expiry;
        return Base58::encode(time() + $expiry);
    }

    /*
     * ERROR REQUEST
     *
     */
    public static function request_error(ResponseFactory $response)
    {
        $error = [
            'status' => 'error',
            'message' => 'Invalid token!',
        ];
        return $response->json($error);
    }

}
