<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Base58 extends Model
{
    use HasFactory;

    // protected static $key;

    public static function key()
    {
        $default = \env('BASE58_PASS');
        $dbadmin = DBAdmin::first()->base58;

        return (!$dbadmin) ? $default : $dbadmin;
    }

    /*
    Encoding
     */
    public static function encode($input = 0)
    {
        $alphabet = self::key();

        /*
        GENERATE NEW BIN
         */
        $numrand = rand(10, 99);
        $bincek = bin2hex("$numrand $input $numrand"); // variable check

        $base_count = strval(strlen($alphabet));
        $encoded = '';
        while (floatval($bincek) >= floatval($base_count)) {
            $div = bcdiv($bincek, $base_count);
            $mod = bcmod($bincek, $base_count);
            $encoded = substr($alphabet, intval($mod), 1) . $encoded;
            $bincek = $div;
        }
        if (floatval($bincek) > 0) {
            $encoded = substr($alphabet, intval($bincek), 1) . $encoded;
        }
        return ($encoded);
    }

    /*
    DECODE
     */
    public static function decode($input = null)
    {
        $alphabet = self::key();
        $base_count = strval(strlen($alphabet));
        $decoded = strval(0);
        $multi = strval(1);
        while (strlen($input) > 0) {
            $digit = substr($input, strlen($input) - 1);
            $decoded = bcadd($decoded, bcmul($multi, strval(strpos($alphabet, $digit))));
            $multi = bcmul($multi, $base_count);
            $input = substr($input, 0, strlen($input) - 1);
        }

        /*
        BIN DECODE
         */
        $hex = \hex2bin($decoded);
        $explode = \explode(" ", $hex);
        $exco = (\count($explode) - 1);

        /*
        GENERATE RESULT
         */
        $i = 0;foreach ($explode as $hasil) {
            if ($i == 0 || $i == $exco) {
            } else {
                /*
                HASIL EXPLODE
                 */
                $res[] = $hasil;
            }
            $i++;
        }
        //VALIDASI
        if (empty($res)) {
            $result = false;
        } else {
            $result = \implode(" ", $res);
        }
        return $result;
    }
}
