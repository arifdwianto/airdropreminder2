<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TableNotification extends Model
{
    use HasFactory;
    protected $fillable = [
        'unread',
        'data',
    ];
    protected $table = 'notification';
    protected $appends = ['date', 'data_x'];
    protected $hidden = ['data'];
    // protected $with = ['data'];
    public function getDateAttribute()
    {
        return Carbon::parse($this->created_at)->format('d F Y');
    }
    public function getDataXAttribute()
    {
        try {
            $data = json_decode($this->data);
            return $data;
        } catch (Exception $e) {
            return null;
        }
    }
}
