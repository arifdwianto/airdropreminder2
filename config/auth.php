<?php

return [

    /*
    |
    | Default Base58 string
    |
     */

    'base58key' => 'ABCDEFGHIJKLMNOPQRSTUVWYZ0X123456879abcdefghijklmnopqrstuvwxyz',

    /*
    |
    | ADMIN PASSWORD
    |
     */

    'admin' => 'root123',

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'provider' => 'users',
        ],
    ],

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\User::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],

    'passwords' => [
        'users' => [
            'provider' => 'users',
            'email' => 'auth.emails.password',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
