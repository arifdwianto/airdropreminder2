<?php

namespace Database\Factories;

use App\Models\AirdropModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class AirdropModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AirdropModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'name' => $this->faker->company(),
            'description' => $this->faker->text(),
            'distribution' => $this->faker->dateTimeBetween($startDate = '-1 months', $endDate = '+1 years', $timezone = null),
            'rate' => rand(10, 100),
        ];
    }
}
