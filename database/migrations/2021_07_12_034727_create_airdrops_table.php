<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateAirdropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airdrop', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 100)->unique();
            $table->smallInteger('active')->default(1);
            $table->string('description', 700)->nullable();
            $table->dateTime('distribution');
            $table->bigInteger('rate')->default(100);
            $table->string('links', 2000)->nullable();
            $table->dateTime('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->dateTime('created_at')->useCurrent();
            $table->index(['id', 'created_at', 'name']);
        });
        Schema::create('admin', function (Blueprint $table) {
            $table->id();
            $table->string('sitename', 100)->default('AIRDROP REMINDER');
            $table->string('base58', 100);
            $table->string('password', 200);
            $table->dateTime('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->dateTime('created_at')->useCurrent();
        });
        Schema::create('notification', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('unread')->default(1);
            $table->string('message', 700);
            $table->string('data', 700);
            $table->dateTime('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->dateTime('created_at')->useCurrent();
        });
        DB::statement("ALTER TABLE admin comment 'DO NOT CHANGE IT MANUALLY'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airdrops');
        Schema::dropIfExists('admin');
    }
}
