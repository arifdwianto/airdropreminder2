<?php

namespace Database\Seeders;

use App\Models\AirdropModel;
use App\Models\DBAdmin;
use Illuminate\Database\Seeder;

class AirdropSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trong = env("BASE58_PASS");
        $tring = str_shuffle($trong);

        AirdropModel::factory()->count(50)->create();
        DBAdmin::create([
            'base58' => $tring,
            'password' => md5('root123'),
        ]);
    }
}
