$(document).on('DOMContentLoaded', () => {
    this.livewire.on('success', (data) => {
        Swal.fire({
            toast: true,
            showConfirmButton: false,
            position: "top-end",
            timer: 2000,
            icon: "success",
            title: data.message,
        });
    })
})
