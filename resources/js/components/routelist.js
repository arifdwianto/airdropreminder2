import {
    createWebHistory,
    createRouter
} from 'vue-router'

import Home from './Index'
import Edit from './Edit'
import Create from './Add'
import Show from './Show'

// let currentRoute = currentPath

const routes = [{
        path: currentPath + "/../show/:id",
        name: 'show',
        component: Show
    },
    {
        path: currentPath + "/../new",
        name: 'create',
        component: Create
    },
    {
        path: currentPath + "/",
        name: 'home',
        component: Home
    },
]

const router = createRouter({
    base: currentPath,
    history: createWebHistory(),
    routes
})

export default router
