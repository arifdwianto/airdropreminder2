/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


import {
    createApp
} from "vue"

import App from "./components/App.vue"
import router from "./components/routelist.js"


let app = createApp(App)

app.use(router).mount("#vue")
