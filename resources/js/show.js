import {
    createApp
}
from "vue"

import App from "./components/Show.vue"

let app = createApp(App)

app.mount("#vue")
