@extends('layouts.app')

@section('title', 'ADD LIST')

@section('content')

@endsection

@section('vuehead')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.css">
    <script>
        let currentPath = "{{ Request::getRequestUri() }}"
        let siteurl = "{{ url('/') }}"
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/1.0.10/datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>

    <script defer src="{{ asset('js/list.js') }}"></script>
@endsection
{{-- <div id="vue">
    <example-component></example-component>
</div> --}}
