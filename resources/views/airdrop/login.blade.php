@extends('layouts.auth')

@section('bodyclass', 'hold-transition login-page')
@section('appclass', 'login-box')

@section('content')

    <div class="login-logo">
        <a href="../../index2.html"><b>LOGIN</b> AREA</a>
    </div>
    <!-- /.login-logo -->
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Sign in to start your session</p>

            <div id="form">
                @csrf
                <div class="input-group mb-3">
                    {{-- <input type="hidden" name="method_field" value="PUT"> --}}
                    <input type="password" class="form-control" id='password' name="password" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div id="error" class="valid-feedback">aaa</div>
                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember">
                            <label for="remember">
                                Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="button" id="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
            </div>

        </div>
        <!-- /.login-card-body -->
    </div>
    {{-- END CARD LOGIN --}}

@endsection

@section('script')

    <script>
        $pom = function() {
            $data = $("#form :input").serialize()
            console.log($data);
            $('#form :input').prop('disabled', true)

            $req = $.post("{{ route('auth.check') }}", $data, function(data) {
                var obj = JSON.parse(JSON.stringify(data))
                if (obj.status == true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Login Berhasil!',
                    })

                    setTimeout(function() {
                        window.location = "{{ url('/auth?token=') }}" + obj.data.token
                    }, 1000)
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Login Gagal!',
                        text: 'Password tidak valid!',
                    })
                }

            })

            $req.done(function() {
                $('#form :input').prop('disabled', false)
            })
        }

        $("#submit").click($pom)
        $("#password").bind("enterKey", $pom);
        $("#password").keyup(function(e) {
            e.keyCode == 13 ? $(this).trigger("enterKey") : ""
        });
    </script>

@endsection
