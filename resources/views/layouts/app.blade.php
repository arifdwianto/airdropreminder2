@extends('layouts.skeleton')

@section('bodyclass', 'hold-transition sidebar-collapse')
@section('appclass', 'wrapper')

@section('stylesheet')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('cdn/css/tempus-bootstrap.css') }}">
@livewireScripts
@endsection

@section('app')


@include('partials.topnav')

@include('partials.sidebar')

<!-- Main Content -->
<div class="content-wrapper">
<div id="vue">
    @yield('content')
</div>
</div>

{{-- FOOTER --}}
<footer class="main-footer">
@include('partials.footer')
</footer>


@endsection

@section('javascript')


<script>
    var AdminLTEOptions = {
        //Enable sidebar expand on hover effect for sidebar mini
        //This option is forced to true if both the fixed layout and sidebar mini
        //are used together
        sidebarExpandOnHover: false,
        //BoxRefresh Plugin
        enableBoxRefresh: true,
        //Bootstrap.js tooltip
        enableBSToppltip: true
    };

    $width = $('body').width()
    $width > 900 ? $('body').attr('class', 'sidebar-open') : ""
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/fastclick@1.0.6/lib/fastclick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/js/demo.min.js"
integrity="sha512-iE1uPZFfI7lCfuI9YLL+B5ERXMTiZQISWu2WcfuIJe5AYnrVkbbIoN2ojQfVjfFckcSsgcJOXoMvQCjskFZR5Q=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@yield('script')


@endsection
