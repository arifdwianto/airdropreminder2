<!DOCTYPE html>
<html lang="en">

<head>

    @hasSection('title')
        <title>@yield('title') &mdash; ARTESTNET</title>
    @else
        <title>ARTESTNET</title>
    @endif

    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/5.5.2/collection/components/icon/icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/css/adminlte.min.css"
        integrity="sha512-mxrUXSjrxl8vm5GwafxcqTrEwO1/oBNU25l20GODsysHReZo4uhVISzAKzaABH6/tTfAxZrY2FprmeAP5UZY8A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    @yield('stylesheet')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    @yield('vuehead')

</head>

<body class=" @hasSection('bodyclass') @yield('bodyclass') @else hold-transition sidebar-mini @endif ">

<div id="app" class="@yield('appclass')">
@yield('app')
</div>
</body>

</html>

<!-- JAVASCRIPT -->
<script src=" {{ asset('cdn/js/bootstrap.bundle.min.js') }} "></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.1.0/js/adminlte.min.js"></script>
<script src=" {{ asset('cdn/js/sa2.min.js') }} "></script>
@yield('javascript')
