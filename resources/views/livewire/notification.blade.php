<div id="notify" wire:ignore.self>
    <li class="nav-item dropdown">
        <a id="navGo" class="nav-link" data-toggle="dropdown" aria-expanded="false">
            <i class="far fa-bell"></i>
            @if ($count != 0)
                <span class="badge badge-success navbar-badge">{{ $count }}</span>
            @endif
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            @if ($dataNotif != '[]')
                <span class="dropdown-item dropdown-header">
                    @if ($getUnread != 0)
                        {{ $getUnread }} New Notifications
                    @else
                        Notifications
                    @endif
                </span>
                <div class="dropdown-divider"></div>
                @foreach ($dataNotif as $item)
                    <div class="ml-2">
                        <a href="{{ url('/show/' . $item->data_x->id) }}"
                            wire:click="unreadData({{ $item->id }})">
                            @if ($item->unread != 0)
                                <span class="text-bold text-dark">{{ $item->message }}</span>
                            @else
                                <span class="text-secondary">
                                    {{ $item->message }}
                                </span>
                            @endif
                            <br>
                            <small class="text-secondary">[ {{ $item->date }} ]</small>
                        </a>
                    </div>
                    <div class="dropdown-divider"></div>
                @endforeach
                <a href="{{ url('/notifications') }}" class="dropdown-item dropdown-footer">See All Notifications</a>

                <a wire:click="markAll" href="#" class="dropdown-item dropdown-footer">
                    Mark All as Read
                </a>
            @else
                <span class="dropdown-item dropdown-header">
                    You doesnt have notifications
                </span>
            @endif
        </div>
    </li>
</div>

<script>
    $('#navGo').one('click', (e) => {
        // Livewire.emit('count', 0)
        e.stopPropagation()
        @this.readNewNotif()
    })
    $(document).on('DOMContentLoaded', () => {
        this.Livewire.on('asyncDone', () => {
            $('#navGo').dropdown('toggle');
        })
    })
</script>
