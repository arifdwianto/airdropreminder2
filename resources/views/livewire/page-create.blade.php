<div>
    {{-- Success is as dangerous as failure. --}}
    <div class="container-fluid">
        <div class="card" x-data="dataLink()">
            <div class="card-header bg-indigo">
                <span class="text-bold"> (+) New Project </span>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Name</label>
                        <input type="text" wire:model.defer="name" placeholder="Airdrop Name" class="form-control">
                        @error('name')
                            <span class="has-error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="">Distribution Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="far fa-calendar-alt"></i>
                                </span>
                            </div>
                            <input id="pickdate" style="{cursor: default}" type="text" class="form-control"
                                placeholder="Select Date" readonly>
                        </div>
                        @error('distribution_date')
                            <span class="has-error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="">Description</label>
                        <textarea wire:model.defer="description" rows="2" class="form-control"
                            placeholder="Description"></textarea>
                        @error('description')
                            <span class="has-error text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="">Links</label>
                        <template x-for="(parent,index) in onelinks">
                            <div class="row">
                                <div class="col-4">
                                    <select x-model="parent.social" class="form-control">
                                        <template x-for="(link,indexlinks) in links" :key="link">
                                            <option x-model="link.name" x-text="link.text">
                                            </option>
                                        </template>
                                    </select>
                                </div>
                                <div class="col-8 mb-1">
                                    <input x-model="parent.value" class="form-control" type="text"
                                        :placeholder="getPlaceholder(parent.social)[0].placeholder">
                                </div>
                            </div>
                        </template>
                        <small x-show="onelinks.length < 6" class="mr-3">
                            <a href="" @click.prevent="addLinks()">+ More Links</a>
                        </small>
                        <small x-show="onelinks.length != 1">
                            <a href="" @click.prevent="removeLinks()"> - Remove One</a>
                        </small>
                    </div>
                    <div class="col">
                        <label for="">Project Rate</label>
                        <div class="row justify-content-between">
                            <div class="form-check">
                                <input wire:model.defer="rate" class="form-check-input bg-danger" type="radio"
                                    name="rate" id="rate1" value="2">
                                <label class="form-check-label text-danger" for="rate1">
                                    Bad
                                </label>
                            </div>
                            <div class="form-check">
                                <input wire:model.defer="rate" class="form-check-input" type="radio" name="rate"
                                    id="rate2" value="60">
                                <label class="form-check-label text-secondary" for="rate2">
                                    Average
                                </label>
                            </div>
                            <div class="form-group float-md-right">
                                <input wire:model.defer="rate" class="form-check-input" type="radio" name="rate"
                                    id="rate3" value="100">
                                <label class="form-check-label text-success" for="rate3">
                                    Good
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col">
                        <button onclick="history.back(-1)" class=" btn btn-default">&laquo; Back</button>
                    </div>
                    <div class="col">
                        <button @click.prevent="alpineEnter()" class="btn float-right btn-primary">Add New
                            Project</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        function dataLink() {
            return {
                onelinks: {!! $onelinks !!},
                links: {!! $links !!},
                addLinks() {
                    let len = this.onelinks.length
                    if (len < 6) {
                        this.onelinks.push({
                            social: 'telegram',
                            value: ''
                        });
                    }
                },
                removeLinks() {
                    let len = this.onelinks.length
                    this.onelinks.splice(len - 1, 1)
                },
                getPlaceholder(value) {
                    let w = this.links.filter(function(el) {
                        return el.name == value
                    })
                    return w
                },
                alpineEnter() {
                    @this.set('onelinks', JSON.stringify(this.onelinks))
                    @this.hitEnter()
                }
            }
        }

        $(document).ready(() => {
            $('#pickdate').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: "dd-mm-yyyy",
                trigger: "#inputDate"
            }).on('change', (e) => {
                @this.set('distribution_date', e.target.value)
            })
            Livewire.on('hitEnter', () => {
                console.log('enter');
            })
        })
    </script>
@endsection
@section('scriptHead')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
@endsection
