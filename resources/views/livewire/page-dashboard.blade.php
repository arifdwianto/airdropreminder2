<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box" onclick="{{ url('/data') }}">
                <span class="info-box-icon bg-success elevation-1"><i class="fas fa-level-up-alt"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Active</span>
                    <span class="info-box-number">
                        {{ $countActive }}
                        <small>Projects</small>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
                <span class="info-box-icon bg-gray elevation-1"><i class="fas fa-ban"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Inactive</span>
                    <span class="info-box-number">
                        {{ $countInactive }}
                        <small>Projects</small>
                    </span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="card p-2">
                <canvas id="dailyStats" />
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Distribution Ongoing
                </div>
                <div class="card-body">
                    @foreach ($distributionList as $data)
                        <div class="border-bottom pb-2 mb-1">
                            <a href="{{ url('/show/' . $data->id) }}" class="text-bold text-dark">
                                {{ $data->name }}
                            </a>
                            <small class="badge badge-{{ $data->div_rate }} ml-3"><i class="far fa-clock"></i>
                                {{ $data->distance }}
                            </small>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.0/chart.min.js"
        integrity="sha512-asxKqQghC1oBShyhiBwA+YgotaSYKxGP1rcSYTDrB0U6DxwlJjU59B67U8+5/++uFjcuVM8Hh5cokLjZlhm3Vg=="
        crossorigin="anonymous" referrerpolicy="no-referrer">
    </script>
    <script>
        let takeDate = []
        let takeValue = []
        let Data = {!! $chart !!}
        let Length = Data.length
        for (i = 0; i < Length; i++) {
            takeDate[i] = Data[i].date
            takeValue[i] = Data[i].value
        }
        let ctx = $('#dailyStats');
        let dailyStats = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: takeDate,
                datasets: [{
                    label: 'Project Added',
                    data: takeValue,
                    backgroundColor: [
                        '#3b82f6',
                        '#f59e0b'
                    ]
                }]
            },
            options: {
                legend: {
                    display: false
                }
            }
        })
    </script>
@endsection
