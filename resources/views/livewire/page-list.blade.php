<div class="container-fluid mt-0">
    <div class="card" x-data="xData()">
        <div class="card-header">
            <div class="row">
                <div class="col-2 col-md-3">
                    <button class="btn btn-block  btn-primary " onclick="window.location.href = '{{ url('/new') }}'">
                        <i class="fas fa-plus d-md-none"></i>
                        <span class="text-bold d-none d-md-block">ADD LISTS</span>
                    </button>
                </div>
                <div class="col-10 offset-md-1 col-md-8">

                    <div class="input-group">
                        <div class="input-group-prepend"><button type="submit" class="btn btn-default"><i
                                    class="fas fa-search"></i></button>
                        </div>
                        <input type="text" wire:model.debounce.1000="search" class="form-control" placeholder="Search">
                    </div>
                </div>
                <div class="text-bold col-2 offset-md-4 mt-2 col-md-1">
                    <h6 class="text-bold align-middle">
                        SORT BY :
                    </h6>
                </div>
                <div class=" col-6 mt-2 col-md-4">
                    <select name="" wire:model="orderVal" class="custom-select">
                        <option value="id">Newest</option>
                        <option value="name">Name</option>
                        <option value="distribution">Distribution Date</option>
                        <option value="created_at">Created Date</option>
                        <option value="active">Status</option>
                    </select>
                </div>
                <div class="col-4 mt-2 col-md-3">
                    <select name="" wire:model="orderBy" class="custom-select">
                        <option value="asc">ASC</option>
                        <option value="desc">DESC</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col" class="d-table-cell" style="width: 300px">Name</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Distribution Date</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                            <tr @click.prevent="goID({{ $item->id }})">
                                <th scope="row"> {{ $item->id }} </th>
                                <td> <span class="text-bold text-{{ $item->div_rate }}">
                                        {{ $item->name }}
                                    </span> </td>
                                <td>{{ $item->mod_date['created_at'] }}</td>
                                <td>{{ $item->mod_date['distribution'] }}</td>
                                <td>
                                    <span class="text-{{ $item->active_attr['class'] }}">
                                        {{ $item->active_attr['text'] }}
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <div class="card-footer">
            {{ $data->links() }}
        </div>
    </div>
</div>

@section('script')
    <script>
        function xData() {
            return {
                goID(val) {
                    window.location.href = '{{ url('/') }}/show/' + val
                }
            }
        }
    </script>
@endsection
