<div>
    @if ($count != 0)
        <div class="container-fluid">
            <div class="card pb-none">
                <div class="card-body">
                    @foreach ($data as $item)
                        <div class="card p-4">
                            <a href="{{ url('/show/' . $item->data_x->id) }}"
                                wire:click="unreadData({{ $item->id }})">
                                @if ($item->unread != 0)
                                    <span class="text-bold text-dark">{{ $item->message }}</span>
                                @else
                                    <span class="text-secondary">
                                        {{ $item->message }}
                                    </span>
                                @endif
                                <br>
                                <small class="text-secondary">[ {{ $item->date }} ]</small>
                            </a>
                        </div>
                    @endforeach
                    <button wire:click.prevent="clearAll" class="btn bg-indigo btn-block d-sm-none">
                        Clear All
                    </button>
                </div>
                <div class="card-footer pb-0">
                    <button wire:click.prevent="clearAll" class="btn bg-indigo d-none d-sm-inline-block">
                        Clear All
                    </button>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    @else
        <div class="container-fluid">
            <div class="card p-5 text-center text-secondary">
                <h4 class="mb-3">Notification is empty. </h4>
                <a href="{{ url('/') }}">&laquo; Back</a>
            </div>
        </div>
    @endif
</div>

@section('script')
    <script>
    </script>
@endsection
