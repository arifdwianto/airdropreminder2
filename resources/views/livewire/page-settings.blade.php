<div>
    <div class="container-fluid" x-data="{menuShow: false, headText: null}">
        <div class="row">
            {{-- SETTINGS --}}
            <div class="col-sm-4">
                <div class="card card-info">
                    <div class="card-header">
                        <h3 class="card-title">
                            Settings Menu
                        </h3>
                    </div>
                    <div class="card-body">
                        <a href="#menus" @click="menuShow = 'changePassword'; headText = 'Change Password' ">&raquo;
                            Change Password</a>
                        <hr>
                        <a href="#menus" @click="menuShow = 'resetData'; headText= 'Reset Data' ">&raquo; Reset Data</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="card" id="menus" x-show="menuShow">
                    <div class="card-header" id="settingsHeader">
                        <div class="card-title">
                            <span x-text="headText" />
                        </div>
                    </div>
                    <div class=" card-body" id="menus">
                        <div x-show="menuShow === 'changePassword'">
                            @include('livewire.settings.change-password')
                        </div>
                        <div x-show="menuShow === 'resetData'">
                            @include('livewire.settings.reset-data')
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        $(document).on('DOMContentLoaded', () => {
            this.livewire.on('success', (data) => {
                Swal.fire({
                    toast: true,
                    showConfirmButton: false,
                    position: "top-end",
                    timer: 2000,
                    icon: "success",
                    title: data.message,
                });
            })
            this.livewire.on('confirm-reset', () => {
                Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-info ml-4',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                }).fire({
                    showCancelButton: true,
                    reverseButtons: true,
                    icon: 'warning',
                    title: 'Reset Data',
                    text: 'This action cannot be undone. Click proceed to continue',
                    cancelButtonText: 'Cancel',
                    confirmButtonText: 'Proceed',
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.doResetData()
                    }
                })
            })
        })
    </script>
@endsection
