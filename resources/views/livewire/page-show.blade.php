<div wire:ignore>
    <div class="container-fluid" x-show="open == 'show'">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header bg-indigo">
                        <h3 class="text-bold p-0">{{ ucwords($data->name) }} [ #{{ $this->getID() }} ]</h3>
                    </div>
                    <div class="card-body pb-1">
                        <div class="card p-2">
                            <b>Description</b>
                            @if ($data->description == null)
                                -
                            @else
                                {{ $data->description }}
                            @endif
                        </div>
                        <div class="card p-2">
                            <b>Created Date</b>
                            {!! $data->mod_date['updated_at'] !!}
                        </div>
                        <div class="card p-2">
                            <b>Distribution Date</b>
                            {!! $data->mod_date['distribution'] !!}
                        </div>

                        <div class="card p-2">
                            <div class="ms-2 me-auto text-bold">Links</div>
                            @foreach ($onelinks as $link)
                                <div>
                                    <i class="fab mr-1 {{ $this->showLinks($link->social)['class'] }}"></i>
                                    <a href="{{ $this->showLinks($link->social, $link->value)['links'] }}">
                                        {{ $this->showLinks($link->social, $link->value)['links'] }}
                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn float-left btn-default" onclick="window.history.back(-1)">&laquo;
                            Back</button>
                        <span class="float-right">
                            <button class="btn btn-primary "
                                onclick="window.location.href='{{ url('/edit/' . $this->data->id) }}'">Edit</button>
                            <button class="btn btn-danger " data-toggle="modal" data-target="#deletes">Delete</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>

    {{-- DELETE --}}
    <div class="modal fade show" aria-hidden="true" id="deletes" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete Project</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body">
                    <p>Are you sure to delete <i class="text-bold">{{ $data->name }}</i> ? <br>
                        This action cannot br undone.</p>
                    <p class="debug-url"></p>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <a class="btn bg-danger btn-ok text-white"
                        wire:click="Deletes({{ $data->id }},'{{ base64_encode($data->name) }}')"
                        data-dismiss="modal">Confirm</a>
                </div>
            </div>
        </div>
    </div>
</div>

@section('script')
    <script>
        $(document).ready(() => {
            $('#pickdate').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: "dd-mm-yyyy",
                trigger: "#inputDate"
            }).on('change', (e) => {
                @this.set('distribution_date', e.target.value)
            })
        })
    </script>
@endsection
@section('scriptHead')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
@endsection
