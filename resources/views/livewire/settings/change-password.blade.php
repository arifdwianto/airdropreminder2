 <form wire:submit.prevent="changePassword">
     <div class="form-group row">
         <label for="inputName" class="col-md-2 col-form-label">Current Password</label>
         <div class="col-md-10">
             <input type="password" class="form-control" placeholder="Current Password" wire:model="currentPassword">
             @error('currentPassword')
                 <span class="has-error text-danger">
                     {{ $message }}
                 </span>
             @enderror
         </div>
     </div>
     <div class="form-group row">
         <label class="col-md-2 col-form-label">New Password</label>
         <div class="col-md-10">
             <input type="password" class="form-control" placeholder="New Password" wire:model="newPassword">
             @error('newPassword')
                 <span class="has-error text-danger">
                     {{ $message }}
                 </span>
             @enderror
         </div>
     </div>
     <div class="form-group row">
         <label for="inputName" class="col-md-2 col-form-label">Confirm New Password</label>
         <div class="col-md-10">
             <input type="password" class="form-control" placeholder="Confirm New Password"
                 wire:model="confirmNewPassword">
             @error('confirmNewPassword')
                 <span class="has-error text-danger">
                     {{ $message }}
                 </span>
             @enderror
         </div>
     </div>
     <div class="form-group row">
         <div class="offset-md-2 col-md-10">
             <button type="submit" class="btn btn-info">Change
                 Password</button>
         </div>
     </div>
 </form>
