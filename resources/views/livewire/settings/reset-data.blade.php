<div class="text-bold mt-0">
    Are you sure?
</div>
Please note. This action will delete all your data.
Input your password below to continue.
<div class="form-group row mt-2">
    <div class="col-md-7 mb-xs-2 col-lg-8">
        <input wire:model="passResetData" type="password" class="input form-control">
        @error('passResetData')
            <span class="has-error text-danger text-small">
                * {{ $message }}
            </span>
        @enderror
    </div>
    <div class="col-md-5 mt-2 col-lg-4  mt-md-0">
        <button wire:click.prevent="checkResetData" class="btn btn-info btn-block">
            Proceed
        </button>
    </div>
</div>
