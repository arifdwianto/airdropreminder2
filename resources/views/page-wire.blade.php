@extends('layouts.app')

@isset($title) @section('title', $title) @endisset

@section('content')
    @isset($name)
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>{{ $name ?? 'Home' }}</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="{{ url('/') }}">Home</a>
                            </li>
                            @isset($pathName)
                                <li class="breadcrumb-item active">{{ $pathName }}</li>
                            @endisset
                        </ol>
                    </div>
                </div>
            </div>
        </section>
    @endisset
    @livewire($pageWire)
@endsection
@section('vuehead')
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@2.8.2/dist/alpine.min.js" defer></script>
    @yield('scriptHead')
@endsection
@section('javascript')
    <script>
        $width = $('body').width()
        $width > 900 ? $('body').attr('class', 'sidebar-open') : ""
        /* LIVEWIRE */
        $(document).on('DOMContentLoaded', () => {
            this.livewire.on('success', (data) => {
                Swal.fire({
                    toast: true,
                    showConfirmButton: false,
                    position: "top-end",
                    timer: 2000,
                    icon: "success",
                    title: data.message,
                })
                if (data.redirect) {
                    setTimeout(function() {
                        window.location = "{{ url('') }}" + data.redirect
                    }, 1500)
                }
            })
        })
    </script>
    @yield('script')
@endsection
