<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-navy bg-navy elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/') }}" class="brand-link text-center">
        {{ \App\Models\DBAdmin::first()->sitename }}
    </a>

    <!-- Sidebar -->
    <div class="sidebar ">
        <!-- Sidebar Menu -->
        <nav class="mt-2 ">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="{{ url('/') }}" class="nav-link">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/new') }}" class="nav-link">
                        <i class="nav-icon fas fa-plus-square"></i>
                        <p>
                            Create
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/data') }}" class="nav-link">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            List
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/setting') }}" class="nav-link">
                        <i class="nav-icon fas fa-tools"></i>
                        <p>
                            Settings
                        </p>
                    </a>
                </li>


                <li class="nav-header"></li>

                <li class="nav-item">
                    <a href="{{ url('/auth/logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
