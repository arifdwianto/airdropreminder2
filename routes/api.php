<?php

use App\Http\Controllers\AirdropCrud;
use App\Http\Middleware\VerifyCsrfToken as MiddlewareVerifyCsrfToken;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::middleware([SessionCheck::class])->group(function (Request $request) {
//     // API SESSION FILTER
//     Route::get('/data', [AirdropCrud::class, 'index']);
// });

Route::group(['prefix' => '/'], function () {

    Route::any('data', [AirdropCrud::class, 'index']);
    Route::any('create', [AirdropCrud::class, 'store']);
    Route::any('show/{id}', [AirdropCrud::class, 'show']);
    Route::any('update/{id}', [AirdropCrud::class, 'update']);
    Route::middleware(VerifyCsrfToken::class)->any('destroy/{id}', [AirdropCrud::class, 'destroy']);
});
