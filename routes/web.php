<?php

use App\Http\Controllers\AirdropController;
use App\Http\Controllers\AirdropCrud;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TesterController;
use App\Http\Middleware\LoginMiddleware;
use App\Http\Middleware\NotLoginMiddleware;
use App\Http\Middleware\SessionCheck;
use App\Models\DBAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*
 *
 * FOR LOGGED USER
 */
Route::middleware(LoginMiddleware::class)->group(function () {
    Route::get('/', [AirdropController::class, 'index']);
    Route::get('/data{any}', [AirdropController::class, 'data_list'])->where('any', '.*');
    Route::get('/new{any}', [AirdropController::class, 'data_create'])->where('any', '.*');
    Route::get('/show/{id}', [AirdropController::class, 'show']);
    Route::get('/edit/{id}', [AirdropController::class, 'update']);
    Route::get('/setting', [AirdropController::class, 'setting']);
    Route::get('/notifications', [AirdropController::class, 'notif']);
});

Route::get('test', [TesterController::class, "index"]);

/*
 * ROUTE FOR NON LOGIN
 *
 */
Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
    /* PUBLIC */
    Route::any('error', [LoginController::class, 'error_json']);

    /* FOR NOT LOGGED  */
    Route::middleware(NotLoginMiddleware::class)->group(function () {
        Route::get("/", [LoginController::class, "index"]);
        Route::any('check', [LoginController::class, "check"])->name('check');
    });

    /* FOR LOGGED */
    Route::middleware(LoginMiddleware::class)->group(function () {
        Route::get('logout', [LoginController::class, 'logout']);
    });
});

/* JSON REQUEST CRUD AIRDROP */
Route::group(['middleware' => SessionCheck::class, 'prefix' => 'airdrop_request'], function () {
    /* THIS ROUTE REQUIRES token Input */
    Route::get('/', [AirdropCrud::class, 'index']);
});
